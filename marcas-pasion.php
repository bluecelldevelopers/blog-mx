<?php include("header.php");?>
<section class="cabecera">
	<div class="imagen">
		<img src="img/cabecera-marcas.jpg" alt="">
	</div>
	<span class="title">MARCAS PASIÓN</span>

</section>
<section class="detail">
	<div class="container">
		<div class="col-left">
			<div class="news">
				<div class="post">
					<div class="img">
						<img src="img/marcas1.jpg" alt="">
					</div>
					<span class="title">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi rhoncus faucibus justo et consequat. </span>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi rhoncus faucibus justo et consequat. Suspendisse tempor scelerisque arcu, eget gravida lectus sodales in. Phasellus semper ex odio, a venenatis sem feugiat auctor. Etiam vel hendrerit dolor.</p>
					<span class="more"><a href="marcas-interna.php">Leer más</a></span>
					<nav class="redes">
						<ul>
							<li><a href=""><i class="icon icon-share"></i></a></li>
							<li><a href=""><i class="icon icon-facebook"></i></a></li>
							<li><a href=""><i class="icon icon-twitter"></i></a></li>
							<li><a href=""><i class="icon icon-gplus"></i></a></li>
							<li><a href=""><i class="icon icon-linkedin"></i></a></li>
							<li><a href=""><i class="icon icon-whatsapp"></i></a></li>
						</ul>
					</nav>	
				</div>
				<div class="post">
					<div class="img">
						<img src="img/marcas2.jpg" alt="">
					</div>
					<span class="title">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi rhoncus faucibus justo et consequat.</span>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi rhoncus faucibus justo et consequat. Suspendisse tempor scelerisque arcu, eget gravida lectus sodales in. Phasellus semper ex odio, a venenatis sem feugiat auctor. Etiam vel hendrerit dolor.</p>
					<span class="more"><a href="marcas-interna.php">Leer más</a></span>
					<nav class="redes">
						<ul>
							<li><a href=""><i class="icon icon-share"></i></a></li>
							<li><a href=""><i class="icon icon-facebook"></i></a></li>
							<li><a href=""><i class="icon icon-twitter"></i></a></li>
							<li><a href=""><i class="icon icon-gplus"></i></a></li>
							<li><a href=""><i class="icon icon-linkedin"></i></a></li>
							<li><a href=""><i class="icon icon-whatsapp"></i></a></li>
						</ul>
					</nav>
					
				</div>
			</div>
			<div class="news">
				<div class="post">
					<div class="img">
						<img src="img/marcas3.jpg" alt="">
					</div>
					<span class="title">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi rhoncus faucibus justo et consequat.</span>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi rhoncus faucibus justo et consequat. Suspendisse tempor scelerisque arcu, eget gravida lectus sodales in. Phasellus semper ex odio, a venenatis sem feugiat auctor. Etiam vel hendrerit dolor.</p>
					<span class="more"><a href="marcas-interna.php">Leer más</a></span>
					<nav class="redes">
						<ul>
							<li><a href=""><i class="icon icon-share"></i></a></li>
							<li><a href=""><i class="icon icon-facebook"></i></a></li>
							<li><a href=""><i class="icon icon-twitter"></i></a></li>
							<li><a href=""><i class="icon icon-gplus"></i></a></li>
							<li><a href=""><i class="icon icon-linkedin"></i></a></li>
							<li><a href=""><i class="icon icon-whatsapp"></i></a></li>
						</ul>
					</nav>
					
				</div>
				<div class="post">
					<div class="img">
						<img src="img/marcas4.jpg" alt="">
					</div>
					<span class="title">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi rhoncus faucibus justo et consequat.</span>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi rhoncus faucibus justo et consequat. Suspendisse tempor scelerisque arcu, eget gravida lectus sodales in. Phasellus semper ex odio, a venenatis sem feugiat auctor. Etiam vel hendrerit dolor.</p>
					<span class="more"><a href="marcas-interna.php">Leer más</a></span>
					<nav class="redes">
						<ul>
							<li><a href=""><i class="icon icon-share"></i></a></li>
							<li><a href=""><i class="icon icon-facebook"></i></a></li>
							<li><a href=""><i class="icon icon-twitter"></i></a></li>
							<li><a href=""><i class="icon icon-gplus"></i></a></li>
							<li><a href=""><i class="icon icon-linkedin"></i></a></li>
							<li><a href=""><i class="icon icon-whatsapp"></i></a></li>
						</ul>
					</nav>
					
				</div>
			</div>
			<div class="news">
				<div class="post">
					<div class="img">
						<img src="img/marcas5.jpg" alt="">
					</div>
					<span class="title">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi rhoncus faucibus justo et consequat.</span>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi rhoncus faucibus justo et consequat. Suspendisse tempor scelerisque arcu, eget gravida lectus sodales in. Phasellus semper ex odio, a venenatis sem feugiat auctor. Etiam vel hendrerit dolor.</p>
					<span class="more"><a href="marcas-interna.php">Leer más</a></span>
					<nav class="redes">
						<ul>
							<li><a href=""><i class="icon icon-share"></i></a></li>
							<li><a href=""><i class="icon icon-facebook"></i></a></li>
							<li><a href=""><i class="icon icon-twitter"></i></a></li>
							<li><a href=""><i class="icon icon-gplus"></i></a></li>
							<li><a href=""><i class="icon icon-linkedin"></i></a></li>
							<li><a href=""><i class="icon icon-whatsapp"></i></a></li>
						</ul>
					</nav>
					
				</div>
				<div class="post">
					<div class="img">
						<img src="img/marcas6.jpg" alt="">
					</div>
					<span class="title">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi rhoncus faucibus justo et consequat.i</span>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi rhoncus faucibus justo et consequat. Suspendisse tempor scelerisque arcu, eget gravida lectus sodales in. Phasellus semper ex odio, a venenatis sem feugiat auctor. Etiam vel hendrerit dolor.</p>
					<span class="more"><a href="marcas-interna.php">Leer más</a></span>
					<nav class="redes">
						<ul>
							<li><a href=""><i class="icon icon-share"></i></a></li>
							<li><a href=""><i class="icon icon-facebook"></i></a></li>
							<li><a href=""><i class="icon icon-twitter"></i></a></li>
							<li><a href=""><i class="icon icon-gplus"></i></a></li>
							<li><a href=""><i class="icon icon-linkedin"></i></a></li>
							<li><a href=""><i class="icon icon-whatsapp"></i></a></li>
						</ul>
					</nav>
					
				</div>
			</div>

		</div><!--  
		--><div class="col-right">
			<span class="title">ArtÍculos de nuestros
			colaboradores</span>
			<img src="img/deporte-detalles.jpg" alt="">
			<div class="experta">
				<img src="img/experta.jpg" alt="">
				<span class="title">María</span>
				<span class="sub">EXPERTA EN RUNNING</span>
				<p>Morbi rhoncus faucibus justo et consequat. </p>
			</div>
			<div class="carrusel-detalle">
				<div class="slide">
					<img src="img/carrusel-detalle.jpg" alt="">
				</div>
				<div class="slide">
					<img src="img/carrusel-detalle.jpg" alt="">
				</div>
				<div class="slide">
					<img src="img/carrusel-detalle.jpg" alt="">
				</div>
			</div>
			<div class="more-sports">
				<span class="title">Más deportes</span>
				<nav>
					<ul>
						<li><a href=""><span>RUNNING</span></a></li>
						<li><a href=""><span>CICLISMO</span></a></li>
						<li><a href=""><span>NATACIÓN</span></a></li>
						<li><a href=""><span>CICLISMO</span></a></li>
						<li><a href=""><span>BALONCESTO</span></a></li>
						<li><a href=""><span>GOLF</span></a></li>
					</ul>
				</nav>
			</div>
		</div>
		<div class="paginador">
			<nav class="page-number">
					<ul>
						<li><a href="" title=""><</a></li>
						<li class="active"><a href="" title="">1</a></li>
						<li><a href="" title="">2</a></li>
						<li><a href="" title="">3</a></li>
						<li><a href="" title="">4</a></li>
						<li><a href="" title="">5</a></li>
						<li><a href="" title="">></a></li>
					</ul>
				</nav>
		</div>
	</div>
</section>
<?php include("footer.php");?>
	
