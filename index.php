<?php include("header.php");?>
<section class="sliders">
	<div class="slide-banner">
		<div class="slide">
			<img src="img/gallery.jpg" alt="" class="backimg">
			<div class="text">
				<span>Entrenar a 2600 metros  de altura</span>
				<span class="link"><a href="">Leer más</a></span>
			</div>
		</div>
		<div class="slide">
			<img src="img/gallery.jpg" alt="" class="backimg">
			<div class="text">
				<span>adsd</span>
				<span class="link"><a href="">Leer más</a></span>
			</div>
		</div>
		<div class="slide">
			<img src="img/gallery.jpg" alt="" class="backimg">
			<div class="text">
				<span>adsd</span>
				<span class="link"><a href="">Leer más</a></span>
			</div>
		</div>
		<div class="slide">
			<img src="img/gallery.jpg" alt="" class="backimg">
			<div class="text">
				<span>adsd</span>
				<span class="link"><a href="">Leer más</a></span>
			</div>
		</div>
		<div class="slide">
			<img src="img/gallery.jpg" alt="" class="backimg">
			<div class="text">
				<span>adsd</span>
			</div>
		</div>
	</div>
	<div class="cover-slider-sportbanner">
		<div class="sportbanner">
			<div class="slide">
				<div class="container">
					<a href="deportes.php">
						<img src="img/ciclismo.png" alt="">
						<span>Ciclismo</span>
					</a>
				</div>
			</div>
			<div class="slide">
				<div class="container">
					<a href="deportes.php">
						<img src="img/basket.png" alt="">
						<span>Running</span>
					</a>
				</div>	
			</div>
			<div class="slide">
				<div class="container">
					<a href="deportes.php">
						<img src="img/basket.png" alt="">
						<span>Fitness</span>
					</a>
				</div>	
			</div>
			<div class="slide">
				<div class="container">
					<a href="deportes.php">
						<img src="img/basket.png" alt="">
						<span>Nutrición y suplementos</span>
					</a>
				</div>	
			</div>
			<div class="slide">
				<div class="container">
					<a href="deportes.php">
						<img src="img/basket.png" alt="">
						<span>Deportes en la Montaña</span>
					</a>
				</div>	
			</div>
			<div class="slide">
				<div class="container">
					<a href="deportes.php">
						<img src="img/basket.png" alt="">
						<span>Natación y Aquagym</span>
					</a>
				</div>	
			</div>
			<div class="slide">
				<div class="container">
					<a href="deportes.php">
						<img src="img/basket.png" alt="">
						<span>Deportes en el mar</span>
					</a>
				</div>	
			</div>
			<div class="slide">
				<div class="container">
					<a href="deportes.php">
						<img src="img/basket.png" alt="">
						<span>Caminata</span>
					</a>
				</div>	
			</div>

			<div class="slide">
				<div class="container">
					<a href="deportes.php">
						<img src="img/basket.png" alt="">
						<span>Equitación</span>
					</a>
				</div>	
			</div>
			
			<div class="slide">
				<div class="container">
					<a href="deportes.php">
						<img src="img/futbol.png" alt="">
						<span>Pesca</span>
					</a>
				</div>
			</div>
			<div class="slide">
				<div class="container">
					<a href="deportes.php">
						<img src="img/basket.png" alt="">
						<span>Golf</span>
					</a>
				</div>	
			</div>
			<div class="slide">
				<div class="container">
					<a href="deportes.php">
						<img src="img/basket.png" alt="">
						<span>Escalada y Alpinismo</span>
					</a>
				</div>	
			</div>
			<div class="slide">
				<div class="container">
					<a href="deportes.php">
						<img src="img/basket.png" alt="">
						<span>Deportes de Raqueta</span>
					</a>
				</div>	
			</div>
			<div class="slide">
				<div class="container">
					<a href="deportes.php">
						<img src="img/natacion.png" alt="">
						<span>patines, skate, patín del diablo</span>
					</a>
				</div>
			</div>
			<div class="slide">
				<div class="container">
					<a href="deportes.php">
						<img src="img/futbol.png" alt="">
						<span>Deportes en equipo</span>
					</a>
				</div>
			</div>
			<div class="slide">
				<div class="container">
					<a href="deportes.php">
						<img src="img/futbol.png" alt="">
						<span>Caza</span>
					</a>
				</div>
			</div>
			<div class="slide">
				<div class="container">
					<a href="deportes.php">
						<img src="img/futbol.png" alt="">
						<span>Boxeo y Artes Marciales</span>
					</a>
				</div>
			</div>
			<div class="slide">
				<div class="container">
					<a href="deportes.php">
						<img src="img/futbol.png" alt="">
						<span>Deportes de precisión</span>
					</a>
				</div>
			</div>
		</div>
	</div>

</section>


<section class="select">
	<div class="container">
		<form action="">
			<select  class="">
				<option value="" selected="selected">Selecciona un deporte</option>
				<option value="">Futbol</option>
				<option value="">Ciclismo</option>
				<option value="">Atletismo</option>
			</select>
		</form>
	</div>
</section>
<section class="news">
	<div class="container">
		<div class="post">
			<div class="img">
				<img src="img/blog1.jpg" alt="">
			</div>
			<span class="title">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi</span>
			<span class="more">Leer más</span>
			<nav class="redes">
				<ul>
					<li><a href=""><i class="icon icon-share"></i></a></li>
					<li><a href=""><i class="icon icon-facebook"></i></a></li>
					<li><a href=""><i class="icon icon-twitter"></i></a></li>
					<li><a href=""><i class="icon icon-gplus"></i></a></li>
					<li><a href=""><i class="icon icon-linkedin"></i></a></li>
					<li><a href=""><i class="icon icon-whatsapp"></i></a></li>
				</ul>
			</nav>
			<div class="deporte">
				<img src="img/icon-futbol.png" alt="">
			</div>
		</div><!--
		--><div class="post">
			<div class="img">
				<img src="img/blog2.jpg" alt="">
			</div>
			<span class="title">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi</span>
			<span class="more">Leer más</span>
			<nav class="redes">
				<ul>
					<li><a href=""><i class="icon icon-share"></i></a></li>
					<li><a href=""><i class="icon icon-facebook"></i></a></li>
					<li><a href=""><i class="icon icon-twitter"></i></a></li>
					<li><a href=""><i class="icon icon-gplus"></i></a></li>
					<li><a href=""><i class="icon icon-linkedin"></i></a></li>
					<li><a href=""><i class="icon icon-whatsapp"></i></a></li>
				</ul>
			</nav>
			<div class="deporte">
				<img src="img/icon-ciclismo.png" alt="">
			</div>
		</div><!--
		--><div class="post">
			<div class="img">
				<img src="img/blog3.jpg" alt="">
			</div>
			<span class="title">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi</span>
			<span class="more">Leer más</span>
			<nav class="redes">
				<ul>
					<li><a href=""><i class="icon icon-share"></i></a></li>
					<li><a href=""><i class="icon icon-facebook"></i></a></li>
					<li><a href=""><i class="icon icon-twitter"></i></a></li>
					<li><a href=""><i class="icon icon-gplus"></i></a></li>
					<li><a href=""><i class="icon icon-linkedin"></i></a></li>
					<li><a href=""><i class="icon icon-whatsapp"></i></a></li>
				</ul>
			</nav>
			<div class="deporte">
				<img src="img/icon-atletismo.png" alt="">
			</div>
		</div>
	</div>
</section>
<?php include("footer.php");?>
	
