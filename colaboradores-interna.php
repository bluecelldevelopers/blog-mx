<?php include("header.php");?>
<section class="cabecera">
	<div class="imagen">
		<img src="img/colaboradores.jpg" alt="">
	</div>
	<span class="title">Colaboradores</span>

</section>
<section class="detail">
	<div class="container">
			<div class="autor">
				<div class="left-cont">
					<div class="foto">
						<img src="img/luis.jpg" alt="">
					</div><!--
					--><div class="biografia">
						<span class="nombre">Luis</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam fringilla enim vel felis posuere elementum. Nulla luctus tempor dignissim. Nunc felis sapien, aliquet ac odio tristique, posuere suscipit neque. Praesent pellentesque imperdiet quam, ut porttitor erat dapibus ac. Etiam mattis nisi nec nulla malesuada bibendum.</p>
						<i class="icon icon-linkedin-circled"></i>
					</div>
				</div><!--
				--><div class="right-cont">
					<i class= "icon icon-calendar"></i>
					<span class="title">Agenda tu cita con Luis</span>
					<form action="">
						<input type="text" placeholder="Nombre">
						<input type="text" placeholder="Correo">
						<select  class="">
							<option value="" selected="selected">Selecciona un deporte</option>
							<option value="">Futbol</option>
							<option value="">Ciclismo</option>
							<option value="">Atletismo</option>
						</select>
						<input type="text" placeholder="dd/mm/aaaa">
						<input type="submit" value="Agendar">

					</form>
				</div>
		</div>
	</div>
	<div class="contenedor-articulos">
		<span class="articulos">Artículos recientes del colaborador</span>
	</div>
</section>
<section class="news">
	<div class="container">
		<div class="post">
			<div class="img">
				<img src="img/blog1.jpg" alt="">
			</div>
			<span class="title">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </span>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi rhoncus faucibus justo et consequat. Suspendisse tempor scelerisque arcu, eget gravida lectus sodales in. Phasellus semper ex odio, a venenatis sem feugiat auctor. Etiam vel hendrerit dolor.</p>
			<span class="more">Leer más</span>
			<nav class="redes">
				<ul>
					<li><a href=""><i class="icon icon-share"></i></a></li>
					<li><a href=""><i class="icon icon-facebook"></i></a></li>
					<li><a href=""><i class="icon icon-twitter"></i></a></li>
					<li><a href=""><i class="icon icon-gplus"></i></a></li>
					<li><a href=""><i class="icon icon-linkedin"></i></a></li>
					<li><a href=""><i class="icon icon-whatsapp"></i></a></li>
				</ul>
			</nav>
			<div class="deporte">
				<img src="img/icon-futbol.png" alt="">
			</div>
		</div><!--
		--><div class="post">
			<div class="img">
				<img src="img/blog1.jpg" alt="">
			</div>
			<span class="title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi rhoncus faucibus justo et consequat. Suspendisse tempor scelerisque arcu, eget gravida lectus sodales in. Phasellus semper ex odio, a venenatis sem feugiat auctor. Etiam vel hendrerit dolor.</p>
			<span class="more">Leer más</span>
			<nav class="redes">
				<ul>
					<li><a href=""><i class="icon icon-share"></i></a></li>
					<li><a href=""><i class="icon icon-facebook"></i></a></li>
					<li><a href=""><i class="icon icon-twitter"></i></a></li>
					<li><a href=""><i class="icon icon-gplus"></i></a></li>
					<li><a href=""><i class="icon icon-linkedin"></i></a></li>
					<li><a href=""><i class="icon icon-whatsapp"></i></a></li>
				</ul>
			</nav>
			<div class="deporte">
				<img src="img/icon-ciclismo.png" alt="">
			</div>
		</div><!--
		--><div class="post">
			<div class="img">
				<img src="img/blog1.jpg" alt="">
			</div>
			<span class="title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi rhoncus faucibus justo et consequat. Suspendisse tempor scelerisque arcu, eget gravida lectus sodales in. Phasellus semper ex odio, a venenatis sem feugiat auctor. Etiam vel hendrerit dolor.</p>
			<span class="more">Leer más</span>
			<nav class="redes">
				<ul>
					<li><a href=""><i class="icon icon-share"></i></a></li>
					<li><a href=""><i class="icon icon-facebook"></i></a></li>
					<li><a href=""><i class="icon icon-twitter"></i></a></li>
					<li><a href=""><i class="icon icon-gplus"></i></a></li>
					<li><a href=""><i class="icon icon-linkedin"></i></a></li>
					<li><a href=""><i class="icon icon-whatsapp"></i></a></li>
				</ul>
			</nav>
			<div class="deporte">
				<img src="img/icon-atletismo.png" alt="">
			</div>
		</div>

	</div>
	<div class="container">
		<div class="post">
			<div class="img">
				<img src="img/blog1.jpg" alt="">
			</div>
			<span class="title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi rhoncus faucibus justo et consequat. Suspendisse tempor scelerisque arcu, eget gravida lectus sodales in. Phasellus semper ex odio, a venenatis sem feugiat auctor. Etiam vel hendrerit dolor.</p>
			<span class="more">Leer más</span>
			<nav class="redes">
				<ul>
					<li><a href=""><i class="icon icon-share"></i></a></li>
					<li><a href=""><i class="icon icon-facebook"></i></a></li>
					<li><a href=""><i class="icon icon-twitter"></i></a></li>
					<li><a href=""><i class="icon icon-gplus"></i></a></li>
					<li><a href=""><i class="icon icon-linkedin"></i></a></li>
					<li><a href=""><i class="icon icon-whatsapp"></i></a></li>
				</ul>
			</nav>
			<div class="deporte">
				<img src="img/icon-futbol.png" alt="">
			</div>
		</div><!--
		--><div class="post">
			<div class="img">
				<img src="img/blog1.jpg" alt="">
			</div>
			<span class="title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi rhoncus faucibus justo et consequat. Suspendisse tempor scelerisque arcu, eget gravida lectus sodales in. Phasellus semper ex odio, a venenatis sem feugiat auctor. Etiam vel hendrerit dolor.</p>
			<span class="more">Leer más</span>
			<nav class="redes">
				<ul>
					<li><a href=""><i class="icon icon-share"></i></a></li>
					<li><a href=""><i class="icon icon-facebook"></i></a></li>
					<li><a href=""><i class="icon icon-twitter"></i></a></li>
					<li><a href=""><i class="icon icon-gplus"></i></a></li>
					<li><a href=""><i class="icon icon-linkedin"></i></a></li>
					<li><a href=""><i class="icon icon-whatsapp"></i></a></li>
				</ul>
			</nav>
			<div class="deporte">
				<img src="img/icon-ciclismo.png" alt="">
			</div>
		</div><!--
		--><div class="post">
			<div class="img">
				<img src="img/blog1.jpg" alt="">
			</div>
			<span class="title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi rhoncus faucibus justo et consequat. Suspendisse tempor scelerisque arcu, eget gravida lectus sodales in. Phasellus semper ex odio, a venenatis sem feugiat auctor. Etiam vel hendrerit dolor.</p>
			<span class="more">Leer más</span>
			<nav class="redes">
				<ul>
					<li><a href=""><i class="icon icon-share"></i></a></li>
					<li><a href=""><i class="icon icon-facebook"></i></a></li>
					<li><a href=""><i class="icon icon-twitter"></i></a></li>
					<li><a href=""><i class="icon icon-gplus"></i></a></li>
					<li><a href=""><i class="icon icon-linkedin"></i></a></li>
					<li><a href=""><i class="icon icon-whatsapp"></i></a></li>
				</ul>
			</nav>
			<div class="deporte">
				<img src="img/icon-atletismo.png" alt="">
			</div>
		</div>

	</div>

	
	<div class="paginador">
		<nav class="page-number">
			<ul>
				<li><a href="" title=""><</a></li>
				<li class="active"><a href="" title="">1</a></li>
				<li><a href="" title="">2</a></li>
				<li><a href="" title="">3</a></li>
				<li><a href="" title="">4</a></li>
				<li><a href="" title="">5</a></li>
				<li><a href="" title="">></a></li>
			</ul>
		</nav>
	</div>
</section>

<?php include("footer.php");?>
	
