<?php
/*
Template Name: Deportes nuevo
*/

get_header(); 


$arrKeys = array_keys($_GET);
$deporte_min = strtolower($arrKeys[0]);

if ($deporte_min == 'deporte') {
	$deporte_min = $_GET['deporte'];
}

global $wpdb;
$results = $wpdb->get_results('SELECT * FROM wp_terms WHERE slug = "'.$deporte_min.'"', OBJECT);


if (empty($results)) {
	$id_term = 1;
}

$id_term = $results[0]->term_id;

$description_category = $wpdb->get_results('SELECT * FROM wp_term_taxonomy WHERE term_id = "'.$id_term.'"', OBJECT);


$descripcion_c =  $description_category[0]->description;

// Ahora vamos a consultar si es hijo
$parent = 0;
if ($description_category[0]->parent != 0) {
	$args =
	array(
		'post_type' => 'blog',
		'post_status' => 'publish',
		'orderby' 	=> 'date',
		'order' => 'DESC',
		'meta_query' => array(
			'relation' => 'OR',
	    array(
				'key'     => 'tipo',
				'value'   => $id_term,
				'compare' => 'IN',
	    ),
			array(
				'key'     => 'tipo',
				'value'   => $parent,
				'compare' => 'IN',
			),
		),
		'posts_per_page'  => 6,
		'paged' => get_query_var('paged')
	);


	$wp_query = new WP_Query();
	$wp_query->query($args);

	$padre = $wpdb->get_results('SELECT * FROM wp_terms WHERE term_id = "'.$description_category[0]->parent.'"', OBJECT);


	$name_image = $padre[0]->slug;

}elseif ($description_category[0]->parent == 0) {
	$childrens = $wpdb->get_results('SELECT term_id FROM wp_term_taxonomy WHERE 	parent = "'.$id_term.'"');

	foreach ($childrens as $key => $value_childrens) {
		
		$children[] = $value_childrens->term_id;

	}

	$args =
		array(
			'post_type' => 'blog',
			'post_status' => 'publish',
			'orderby' 	=> 'date',
			'order' => 'DESC',
			'meta_query' => array(
				'relation' => 'OR',
		    array(
					'key'     => 'tipo',
					'value'   => $id_term,
					'compare' => 'IN',
		    ),
				array(
					'key'     => 'tipo',
					'value'   => $children,
					'compare' => 'IN',
				),
			),
			'posts_per_page'  => 6,
			'paged' => get_query_var('paged')
		);

		$wp_query = new WP_Query();
		$wp_query->query($args);

		
		$name_image = $results[0]->slug;
}

$name = ucwords($deporte_min);
$name = str_replace('-', ' ', $name);
$nombre_principal = $results[0]->name;


?>
<section class="cabecera">
	<div class="imagen">
		<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/cabecera-<?php echo $name_image; ?>.jpg" alt="">
	</div>
	<span class="title"><?php echo $nombre_principal; ?></span>
	
	<div class="container">
		<div class="deporte">
			<span class="icon-<?php echo $deporte_min; ?> bg"></span>
		</div>
		<div class="textos">
			<p><?php echo $descripcion_c; ?> </p>
		</div>
		<?php
			if (!empty($childrens)) {
		?>
		<nav class="sub-deportes">
      <ul>
				<?php
				foreach ($childrens as $key => $value_children) {
					$name_sub = $wpdb->get_results('SELECT * FROM wp_terms WHERE term_id = "'.$value_children->term_id.'"', OBJECT);
				?>
					<li><a href="<?php echo get_site_url(); ?>/deporte?<?php echo $name_sub[0]->slug; ?>"><span><?php echo $name_sub[0]->name; ?></span><i class="icon-<?php echo $name_sub[0]->slug; ?>"></i></a></li>
				<?php
				}
				?>	
			</ul>
		</nav>
		<?php
			}
		?>
	</div>
</section>
<section class="detail">
	<div class="container">
	<div class="col-left">
		<div class="news">
			<?php
				if ($deporte_min == 'running') {
						$deporte_min = 'sub-running';
				}

				$count = 0;
				while ($wp_query->have_posts()) : $wp_query->the_post();
					
					$id_select = get_the_ID();
					$pod = pods('blog', get_the_ID());
					$tipo = $pod->field('tipo');
		
					// Titulo.
					$count++;
				
					$titulo = $pod->field('titulo');

					$contenido = $pod->field('resumen');
					$image = $pod->field('imagen');
					$tipo = $pod->field('tipo');
					$uri = $image['guid'];

					
					$data_user = get_userdata(get_the_author_ID());
				
					$name_user = $data_user->data->display_name;

				//Cerrramos el while
			?>
				<div class="post">
					<a href="<?php the_permalink(); ?>"><div class="img">
						<img src="<?php echo $uri; ?>" alt="">
					</div></a>
					<a href="<?php the_permalink(); ?>"><span class="title"><?php echo $titulo; ?> </span></a>
					<p><?php echo $contenido ?></p>
					<a href="<?php the_permalink(); ?>"><span class="more">Leer más</span></a>
					<nav class="redes">
						<ul>
							<li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank"><i class="icon icon-facebook"></i></a></li>
							<li><a href="https://twitter.com/intent/tweet?url=<?php the_permalink(); ?>&text=<?php echo $titulo; ?>" target="_blank"><i class="icon icon-twitter"></i></a></li>
							<li><a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank"><i class="icon icon-gplus"></i></a></li>
						</ul>
					</nav>
					<div class="deporte">
						<span class="icon-<?php echo $tipo['slug']; ?> small"></span>
					</div>
				</div>
			<?php

				if($count % 2 == 0)  {
					
					?>
						</div>
						<div class="news">
					<?php
				}

			endwhile;


			?>
			</div>
		</div><!--  
		--><div class="col-right">
			<span class="title">ArtÍculos de nuestros
			colaboradores</span>
			<a href="<?php echo get_site_url(); ?>/colaboradores"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/deporte-detalles.jpg" alt=""></a>
			<?php 
				// Cargamos los datos publicados.
				$post =
					array(
						'post__not_in' => array($id_select),
						'post_type' => 'blog',
						'post_status' => 'publish',
						'orderby' 	=> 'date',
						'order' => 'DESC',
						'showposts'  => 2,
					);

					$wp_query_post = new WP_Query();
					$wp_query_post->query($post);

					while ($wp_query_post->have_posts()) : $wp_query_post->the_post();
						$pod = pods('blog', get_the_ID());
						$tipo = $pod->field('tipo');
			
						// Titulo.
					
						$titulo = $pod->field('titulo');

						$contenido = $pod->field('resumen');
						$contenido =  substr($contenido, 0, 56);
						$image = $pod->field('imagen');
						$uri = $image['guid'];

			?>
				<a href="<?php the_permalink(); ?>"><div class="experta">
					<img src="<?php echo $uri; ?>" alt="">
					<span class="title"><?php echo $titulo; ?></span>
					<p><?php echo $contenido; ?>...</p>
				</div>
				</a>
			<?php
					endwhile;
			?>
			<div class="more-sports">
			<?php
				$mas_deportes = $wpdb->get_results('SELECT name, slug FROM wp_terms WHERE term_id IN (1,47, 26, 46, 48,22)');
			?>
				<span class="title">Más deportes</span>
				<nav>
					<ul>	
						<?php 
						foreach ($mas_deportes as $key => $value) {							
						?>
						<li><a href="<?php echo get_site_url(); ?>/deporte?<?php echo $value->slug; ?>"><span><?php echo $value->name; ?></span></a></li>
						<?php
							}
						?>
					</ul>
				</nav>
			</div>
		</div>
	<div class="paginador">
		<nav class="page-number">
			<ul>
				<?php echo wp_pagenavi(); ?>
			</ul>
		</nav>
	</div>
	</div>
</section>
<?php get_footer(); ?> 
