<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage decathlon
 * @since 1.0
 * @version 1.0
 */
global $wpdb;
$results = $wpdb->get_results( 'SELECT * FROM wp_term_taxonomy WHERE parent = 0', OBJECT);


foreach ($results as $key => $value) {
	
	$id = $value->term_taxonomy_id;
	
	$query = $wpdb->get_results( 'SELECT * FROM wp_terms WHERE term_id = '.$id.'', OBJECT);
	$name_category = $query[0]->name;
	
	break;
}
?>
<!DOCTYPE html>
<html lang="es" xml:lang="es" >
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<?php do_action('wp_head'); ?>

	<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0, minimum-scale=1.0">
  <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/jquery-ui.theme.min.css">
	<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/jquery-ui.min.css">
  <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/main.css">
</head>
<body>
<div class="wrapper">
	<header>
			<div class="container">
				<div class="logo">
						<a href="<?php echo get_site_url(); ?>"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo.jpg" alt=""></a>
				</div>
				<div class="box">
					<span></span>
					<span></span>
					<span></span>
				</div>
				<nav class="menu">
					<ul>
						<li class="menu-up"><span>Deportes<i class="icon-down"></i></span>
						</li><!--
						--><li><a href="<?php echo get_site_url(); ?>/nuestrosvalores"><span>Nuestros Valores</span></a></li><!--
						--><li><a href="<?php echo get_site_url(); ?>/marca-pasion"><span>Marcas Pasión</span></a></li><!--
						--><li><a href="<?php echo get_site_url(); ?>/colaboradores"><span>Colaboradores</span></a></li>
					</ul>
				</nav>
			</div>	
			<nav class="submenu">
				<ul>
					<li class="movil"><span class="back"><i class="icon-left"></i>Volver</span></li>
					<li><a href="<?php echo get_site_url(); ?>/deporte?running"><i class="icon-running"></i><span>Running</span></a></li><!--
					--><li><a href="<?php echo get_site_url(); ?>/deporte?ciclismo"><i class="icon-ciclismo"></i><span>Ciclismo</span></a></li><!--
					--><li><a href="<?php echo get_site_url(); ?>/deporte?fitness"><i class="icon-fitness"></i><span>Fitness</span></a></li><!--
					--><li><a href="<?php echo get_site_url(); ?>/deporte?nutricion-suplementos"><i class="icon-nutricion-suplementos"></i><span>Nutrición y suplementos</span></a></li><!--
					--><li><a href="<?php echo get_site_url(); ?>/deporte?deportes-montana"><i class="icon-deportes-montana"></i><span>Deportes en la Montaña</span></a></li><!--
					--><li><a href="<?php echo get_site_url(); ?>/deporte?natacion-y-aguagym"><i class="icon-natacion"></i><span>Natación y Aquagym</span></a></li><!--
					--><li><a href="<?php echo get_site_url(); ?>/deporte?deportes-en-el-mar"><i class="icon-deportes-en-el-mar"></i><span>Deportes en el mar</span></a></li><!--
					--><li><a href="<?php echo get_site_url(); ?>/deporte?caminata"><i class="icon-caminata"></i><span>Caminata</span></a></li><!--
					--><li><a href="<?php echo get_site_url(); ?>/deporte?equitacion"><i class="icon-equitacion"></i><span>Equitación</span></a></li><!--
					--><li><a href="<?php echo get_site_url(); ?>/deporte?pesca"><i class="icon-pesca"></i><span>Pesca</span></a></li><!--
					--><li><a href="<?php echo get_site_url(); ?>/deporte?golf"><i class="icon-golf"></i><span>Golf</span></a></li><!--
					--><li><a href="<?php echo get_site_url(); ?>/deporte?escalada-y-alpinismo"><i class="icon-escalada-y-alpinismo"></i><span>Escalada y Alpinismo</span></a></li><!--
					--><li><a href="<?php echo get_site_url(); ?>/deporte?deportes-raqueta"><i class="icon-deportes-raqueta"></i><span>Deportes de Raqueta</span></a></li><!--
					--><li><a href="<?php echo get_site_url(); ?>/deporte?patines"><i class="icon-patines"></i><span>Patines, Skate Patín del diablo</span></a></li><!--
					--><li><a href="<?php echo get_site_url(); ?>/deporte?deportes-en-equipo"><i class="icon-deportes-en-equipo"></i><span>Deportes en equipo</span></a></li><!--
					--><li><a href="<?php echo get_site_url(); ?>/deporte?caza"><i class="icon-caza"></i><span>Caza</span></a></li><!--
					--><li><a href="<?php echo get_site_url(); ?>/deporte?boxeo-artes-marciales"><i class="icon-boxeo-artes-marciales"></i><span>Boxeo | Artes Marciales</span></a></li><!--
					--><li><a href="<?php echo get_site_url(); ?>/deporte?precision"><i class="icon-precision"></i><span>Deportes de precisión</span></a></li>
				</ul>
			</nav>
	</header>

