<?php include("header.php");?>
<section class="cabecera">
	<div class="imagen">
		<img src="img/cabecera-futbol.jpg" alt="">
	</div>
	<span class="title">Fútbol</span>
	
	<div class="container">
		<div class="deporte">
			<img src="img/futbol-logo.jpg" alt="">
		</div>
		<div class="textos">
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla </p>
		</div>
	</div>
</section>
<section class="detail">
	<div class="container">
		<div class="contenido">
			<img src="img/futbol-detalle.jpg" alt="">
			<span class="title">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi rhoncus faucibus justo et consequat.</span>

			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut laoreet pulvinar purus ac dictum. Donec ac aliquam sem. In viverra orci vel enim lobortis malesuada. Ut felis neque, pulvinar ut pretium eget, sodales ac lectus. Donec vel felis et leo mattis posuere. Cras id tristique eros, maximus facilisis nisi. Nulla blandit aliquet urna, et gravida nunc suscipit vel. Duis velit sem, scelerisque vitae lorem ut, euismod mattis lorem. Cras et laoreet purus.</p>

			<p>Phasellus purus orci, ornare in ipsum ut, gravida iaculis magna. Vivamus mattis nisi id neque elementum fermentum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed sit amet euismod dui. Aliquam rhoncus laoreet est vel dignissim. Proin in interdum lectus. Quisque bibendum, nisi quis elementum commodo, dolor sem aliquet justo, in dictum quam sem in leo. Mauris feugiat tellus fermentum iaculis ultrices. Donec et mi eget quam tristique sagittis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>

			<p>Curabitur lacinia finibus lectus, rutrum pretium purus vulputate sit amet. Interdum et malesuada fames ac ante ipsum primis in faucibus. In blandit malesuada magna. Quisque eu mauris in leo dictum dapibus sit amet a sem. Aenean porta auctor arcu. Nulla pulvinar magna ut ultrices egestas. Fusce quis ex in massa viverra aliquam. Etiam maximus aliquam sem, sed aliquam felis iaculis non. Aenean finibus, enim eget pulvinar dapibus, purus sapien hendrerit erat, eu consectetur neque ipsum non magna. Vestibulum nisi nibh, lacinia at sem et, convallis tempus felis. Curabitur non orci cursus, pharetra dolor at, malesuada velit. Pellentesque sed dui justo. Cras cursus ullamcorper egestas. Curabitur at iaculis odio.</p>
			<nav class="redes">
				<ul>
					<li><a href=""><i class="icon icon-share"></i></a></li>
					<li><a href=""><i class="icon icon-facebook"></i></a></li>
					<li><a href=""><i class="icon icon-twitter"></i></a></li>
					<li><a href=""><i class="icon icon-gplus"></i></a></li>
					<li><a href=""><i class="icon icon-linkedin"></i></a></li>
					<li><a href=""><i class="icon icon-whatsapp"></i></a></li>
				</ul>
			</nav>
		</div>
		<div class="autor">
			<div class="left-cont">
				<div class="foto">
					<img src="img/luis.jpg" alt="">
				</div><!--
				--><div class="biografia">
					<span class="nombre">Luis</span>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam fringilla enim vel felis posuere elementum. Nulla luctus tempor dignissim. Nunc felis sapien, aliquet ac odio tristique, posuere suscipit neque. Praesent pellentesque imperdiet quam, ut porttitor erat dapibus ac. Etiam mattis nisi nec nulla malesuada bibendum.</p>
					<i class="icon icon-linkedin-circled"></i>
				</div>
			</div><!--
			--><div class="right-cont">
				<i class= "icon icon-calendar"></i>
				<span class="title">Agenda tu cita con Luis</span>
				<form action="">
					<input type="text" placeholder="Nombre">
					<input type="text" placeholder="Correo">
					<select  class="">
						<option value="" selected="selected">Selecciona un deporte</option>
						<option value="">Futbol</option>
						<option value="">Ciclismo</option>
						<option value="">Atletismo</option>
					</select>
					<input type="text" placeholder="dd/mm/aaaa">
					<input type="submit" value="Agendar">

				</form>
			</div>
		</div>
	</div>
</section>
<?php include("footer.php");?>
	
