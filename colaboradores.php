<?php include("header.php");?>
<section class="cabecera">
	<div class="imagen">
		<img src="img/colaboradores.jpg" alt="">
	</div>
	<span class="title">Colaboradores</span>

</section>
<section class="news">
	<div class="container">
		<div class="post">
			<div class="img">
				<img src="img/colaborador1.jpg" alt="">
			</div>
			<span class="colaborador-nombre">Luis</span>
			<p>Suspendisse tempor scelerisque arcu, eget gravida lectus sodales in. Phasellus semper ex odio, a venenatis sem feugiat auctor.</p>
			<span class="more"><a href="colaboradores-interna.php">Leer más</a></span>
			<nav class="redes">
				<ul>
					<li><a href=""><i class="icon icon-facebook"></i></a></li>
					<li><a href=""><i class="icon icon-twitter"></i></a></li>
					<li><a href=""><i class="icon icon-gplus"></i></a></li>
				</ul>
			</nav>
			<div class="deporte">
				<img src="img/icon-futbol.png" alt="">
			</div>
		</div><!--
		--><div class="post">
			<div class="img">
				<img src="img/colaborador2.jpg" alt="">
			</div>
			<span class="colaborador-nombre">Laura</span>
			<p>Suspendisse tempor scelerisque arcu, eget gravida lectus sodales in. Phasellus semper ex odio, a venenatis sem feugiat auctor.</p>
			<span class="more"><a href="colaboradores-interna.php">Leer más</a></span>
			<nav class="redes">
				<ul>
					<li><a href=""><i class="icon icon-facebook"></i></a></li>
					<li><a href=""><i class="icon icon-twitter"></i></a></li>
					<li><a href=""><i class="icon icon-gplus"></i></a></li>
				</ul>
			</nav>
			<div class="deporte">
				<img src="img/icon-ciclismo.png" alt="">
			</div>
		</div><!--
		--><div class="post">
			<div class="img">
				<img src="img/colaborador3.jpg" alt="">
			</div>
			<span class="colaborador-nombre">Carlos</span>
			<p>Suspendisse tempor scelerisque arcu, eget gravida lectus sodales in. Phasellus semper ex odio, a venenatis sem feugiat auctor.</p>
			<span class="more"><a href="colaboradores-interna.php">Leer más</a></span>
			<nav class="redes">
				<ul>
					<li><a href=""><i class="icon icon-facebook"></i></a></li>
					<li><a href=""><i class="icon icon-twitter"></i></a></li>
					<li><a href=""><i class="icon icon-gplus"></i></a></li>
				</ul>
			</nav>
			<div class="deporte">
				<img src="img/icon-atletismo.png" alt="">
			</div>
		</div>

	</div>
	<div class="container">
		<div class="post">
			<div class="img">
				<img src="img/colaborador4.jpg" alt="">
			</div>
			<span class="colaborador-nombre">Camila</span>
			<p>Suspendisse tempor scelerisque arcu, eget gravida lectus sodales in. Phasellus semper ex odio, a venenatis sem feugiat auctor.</p>
			<span class="more"><a href="colaboradores-interna.php">Leer más</a></span>
			<nav class="redes">
				<ul>
					<li><a href=""><i class="icon icon-facebook"></i></a></li>
					<li><a href=""><i class="icon icon-twitter"></i></a></li>
					<li><a href=""><i class="icon icon-gplus"></i></a></li>
				</ul>
			</nav>
			<div class="deporte">
				<img src="img/icon-futbol.png" alt="">
			</div>
		</div><!--
		--><div class="post">
			<div class="img">
				<img src="img/colaborador5.jpg" alt="">
			</div>
			<span class="colaborador-nombre">Andrés</span>
			<p>Suspendisse tempor scelerisque arcu, eget gravida lectus sodales in. Phasellus semper ex odio, a venenatis sem feugiat auctor.</p>
			<span class="more"><a href="colaboradores-interna.php">Leer más</a></span>
			<nav class="redes">
				<ul>
					<li><a href=""><i class="icon icon-facebook"></i></a></li>
					<li><a href=""><i class="icon icon-twitter"></i></a></li>
					<li><a href=""><i class="icon icon-gplus"></i></a></li>
				</ul>
			</nav>
			<div class="deporte">
				<img src="img/icon-ciclismo.png" alt="">
			</div>
		</div><!--
		--><div class="post">
			<div class="img">
				<img src="img/colaborador6.jpg" alt="">
			</div>
			<span class="colaborador-nombre">Luisa</span>
			<p>Suspendisse tempor scelerisque arcu, eget gravida lectus sodales in. Phasellus semper ex odio, a venenatis sem feugiat auctor.</p>
			<span class="more"><a href="colaboradores-interna.php">Leer más</a></span>
			<nav class="redes">
				<ul>
					<li><a href=""><i class="icon icon-facebook"></i></a></li>
					<li><a href=""><i class="icon icon-twitter"></i></a></li>
					<li><a href=""><i class="icon icon-gplus"></i></a></li>
				</ul>
			</nav>
			<div class="deporte">
				<img src="img/icon-atletismo.png" alt="">
			</div>
		</div>

	</div>
	<div class="container">
		<div class="post">
			<div class="img">
				<img src="img/colaborador7.jpg" alt="">
			</div>
			<span class="colaborador-nombre">Martha</span>
			<p>Suspendisse tempor scelerisque arcu, eget gravida lectus sodales in. Phasellus semper ex odio, a venenatis sem feugiat auctor.</p>
			<span class="more"><a href="colaboradores-interna.php">Leer más</a></span>
			<nav class="redes">
				<ul>
					<li><a href=""><i class="icon icon-facebook"></i></a></li>
					<li><a href=""><i class="icon icon-twitter"></i></a></li>
					<li><a href=""><i class="icon icon-gplus"></i></a></li>
				</ul>
			</nav>
			<div class="deporte">
				<img src="img/icon-futbol.png" alt="">
			</div>
		</div><!--
		--><div class="post">
			<div class="img">
				<img src="img/colaborador1.jpg" alt="">
			</div>
			<span class="colaborador-nombre">Antonio</span>
			<p>Suspendisse tempor scelerisque arcu, eget gravida lectus sodales in. Phasellus semper ex odio, a venenatis sem feugiat auctor.</p>
			<span class="more"><a href="colaboradores-interna.php">Leer más</a></span>
			<nav class="redes">
				<ul>
					<li><a href=""><i class="icon icon-facebook"></i></a></li>
					<li><a href=""><i class="icon icon-twitter"></i></a></li>
					<li><a href=""><i class="icon icon-gplus"></i></a></li>
				</ul>
			</nav>
			<div class="deporte">
				<img src="img/icon-ciclismo.png" alt="">
			</div>
		</div><!--
		--><div class="post">
			<div class="img">
				<img src="img/colaborador8.jpg" alt="">
			</div>
			<span class="colaborador-nombre">David</span>
			<p>Suspendisse tempor scelerisque arcu, eget gravida lectus sodales in. Phasellus semper ex odio, a venenatis sem feugiat auctor.</p>
			<span class="more"><a href="colaboradores-interna.php">Leer más</a></span>
			<nav class="redes">
				<ul>
					<li><a href=""><i class="icon icon-facebook"></i></a></li>
					<li><a href=""><i class="icon icon-twitter"></i></a></li>
					<li><a href=""><i class="icon icon-gplus"></i></a></li>
				</ul>
			</nav>
			<div class="deporte">
				<img src="img/icon-atletismo.png" alt="">
			</div>
		</div>

	</div>
	
	<div class="paginador">
		<nav class="page-number">
			<ul>
				<li><a href="" title=""><</a></li>
				<li class="active"><a href="" title="">1</a></li>
				<li><a href="" title="">2</a></li>
				<li><a href="" title="">3</a></li>
				<li><a href="" title="">4</a></li>
				<li><a href="" title="">5</a></li>
				<li><a href="" title="">></a></li>
			</ul>
		</nav>
	</div>
</section>

<?php include("footer.php");?>
	
