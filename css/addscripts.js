(function($){
	jQuery(document).ready(function() {
     if($('a[data-mailto]').length){
    	$.each($('a[data-mailto]'), function(index, val) {
    	 var uri = $(val).attr('data-mailto');
    	 $(val).attr('href', 'mailto:?subject=Fundación FAES&body='+uri);
    	});   
     }	
	});
})(jQuery)