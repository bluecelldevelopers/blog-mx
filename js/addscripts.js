(function($){
	jQuery(document).ready(function() {
     if($('a[data-mailto]').length){
    	$.each($('a[data-mailto]'), function(index, val) {
    	 var uri = $(val).attr('data-mailto');
    	 $(val).attr('href', 'mailto:?subject=Fundación FAES&body='+uri);
    	});   
     }
     $('.prensa-galeria').slick({
			dots: false,
			infinite: true,
			speed: 500,
			autoplay: false,
			autoplaySpeed: 5000,
			adaptiveHeight: true
		});
	 	$('.open-modal').on('click',function(event){
        event.preventDefault();
        $('.modalbox').fadeIn(500);
	        $('.modalbox').find('.vid').show();
	    });
	    $('.close-modal, .modalbox').on('click',function(event){
	        console.log(event.target);
	        if($(event.target).is('.table-cell') || $(event.target).is('.close-modal')){
	        $('.modalbox').fadeOut(500, function(){
	        $('.vid1').hide();
	        });
	        }
	    });
	});
})(jQuery)