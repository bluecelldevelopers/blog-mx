(function($) {
/*functions*/
	$(document).ready(function(){
				var setSlick = '';
		$('.sportbanner .slick-next').on("click",function(){
			clearInterval(setSlick);
		})
		$('.sportbanner .slick-prev').on("click",function(){
			clearInterval(setSlick);
		})
		$('.sportbanner .slick-next').hover(function(){
			$('.sportbanner').slick('slickNext');
			setSlick = setInterval(function(){
				$('.sportbanner').slick('slickNext');
			},100);
		}, function(){
			clearInterval(setSlick);
		});
		$('.sportbanner .slick-prev').hover(function(){
			$('.sportbanner').slick('slickPrev');
			setSlick = setInterval(function(){

				$('.sportbanner').slick('slickPrev');
			},100);
		}, function(){
			clearInterval(setSlick);
		});
	})
	$('.slide-banner').slick({
		infinite: true,
		slidesToShow: 1,
		adaptiveHeight: true,
		arrows: false,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 7000,
		
	});

	$('.sportbanner').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		centerMode: true,
		arrows: true,
		responsive: [{
			breakpoint: 768,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 1
			}
		},{
			breakpoint: 480,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 1
			}
		}]
	});
	$('.carrusel-detalle').slick({
		infinite: true,
		slidesToShow: 1,
		adaptiveHeight: true,
		dots: true,
		arrows: false
	});
	
	$('header .box').on('click', function(event) {
		event.preventDefault();
		$(this).toggleClass('box-close');
		$('.menu').stop().slideToggle(500);
		$('.submenu').fadeOut();
		$('.menu').removeClass('active');

	});
	$('.menu-up').on('click', function(event) {
		event.preventDefault();
		$('.submenu').stop().slideToggle(500);
		$(this).toggleClass('active');
		$('.menu').toggleClass('active');
	});
	$('.sub-up').on('click', function(event) {
		event.preventDefault();
		$(this).find('.internal-menu').slideToggle(500);
		$(this).toggleClass('active');
	});
	$('.back').on('click', function(event) {
		event.preventDefault();
		$('.submenu').stop().slideToggle(500);
		$('.menu').removeClass('active');
	});

	$('.fixed-responsive .share').on('click', function(event) {
		event.preventDefault();
		$('.modalbox[data-modal="share"]').fadeIn(400);
	});

	$('.modalbox,.modalbox .close-modal').on('click', function(event) {
		if($(event.target).is('.close-modal') || $(event.target).is('.table-cell')){
			$('.modalbox').fadeOut(400, function() {
			});
		}
	});

})(jQuery);