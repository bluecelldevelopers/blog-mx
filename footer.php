<footer>
	<div class="container">	
		<div class="links">
			<div class="logo-foot">
				<img src="img/logo-foot.jpg" alt="">
			</div>
			<div class="section">
				<ul>
					<li><a href="http://www.decathlon.com.mx/c/66-nuestras-tiendas" target="_blank"><span>Nuestras tiendas</span></a></li>
					<li><a href="https://trabajaconnosotros.decathlon.mx/" target="_blank"><span>Trabaja con nosotros</span></a></li>
					<li><a href="http://www.decathlon.com.mx/102-innovaciones" target="_blank"><span>Innovación </span></a></li>
					<li><a href="http://www.decathlon.com.mx/c/74-nuestras-marcas-pasion" target="_blank"><span>Nuestras Marcas</span></a></li>
					<li><a href="http://www.decathlon.com.mx/c/72-preguntas-frecuentes" target="_blank"><span>Preguntas Frecuentes</span></a></li>
					<li><a href="http://www.decathlon.com.mx/104-fin-de-temporada" target="_blank"><span>Fin de temporada</span></a></li>
				</ul>
			</div><!-- 
			--><div class="section">
				<ul>
					<li><a href="deportes.php"><span>Ciclismo</span></a></li>
					<li><a href="deportes.php"><span>Running</span></a></li>
					<li><a href="deportes.php"><span>Fitness</span></a></li>
					<li><a href="deportes.php"><span>Nutrición y Suplementos</span></a></li>
					<li><a href="deportes.php"><span>Deportes en la montaña</span></a></li>
					<li><a href="deportes.php"><span>Natación y Aquagym</span></a></li>
				</ul>
			</div><!-- 
			--><div class="section">
				<ul>
					<li><a href="deportes.php"><span>Deportes en el mar</span></a></li>
					<li><a href="deportes.php"><span>Caminata</span></a></li>
					<li><a href="deportes.php"><span>Equitación</span></a></li>
					<li><a href="deportes.php"><span>Pesca</span></a></li>
					<li><a href="deportes.php"><span>Golf</span></a></li>
					<li><a href="deportes.php"><span>Escalada y Alpinismo</span></a></li>
					
				</ul>
			</div><!-- 
			--><div class="section">
				<ul>
					<li><a href=""><span>Deportes de Raqueta</span></a></li>
					<li><a href=""><span>Patines, Skate, Patín del diablo</span></a></li>
					<li><a href=""><span>Deportes en equipo</span></a></li>
					<li><a href=""><span>Caza</span></a></li>
					<li><a href=""><span>Boxeo y Artes Marciales</span></a></li>
					<li><a href=""><span>Deportes de precisión</span></a></li>
				</ul>
			</div>
		</div>
		<div class="welcome">
			<div class="container">
				<span class="title"> RECIBE LAS ÚLTIMAS NOTICIAS</span>
				<form method="post" action="" class="newsletter" style="">
					<div class="">
						
						<input class="newsletter-name" type="text" name="nn" placeholder="Nombre">
					</div>
					<div class="">
						
						<input class="newsletter-email" type="text" placeholder="Correo">
					</div>
					<div class="newsletter-field newsletter-field-button">
						<input class="newsletter-button" type="submit" value="Suscribirme">
					</div>
				</form>
			</div>
		</div>
		<div class="copy">
			<span class="left">© Decathlon 2017 </span>
			<nav>
				<ul>
					<li><a href="menciones.php"><span>Menciones legales</span></a></li>
					<li><a href="politicas.php"><span>Política de privacidad</span></a></li>
					<li><a href="http://decathlon.com.mx/" target="_blank"><span>Decathlon.com.mx</span></a></li>
				</ul>
			</nav>
		</div>
	</div>
</footer>

	<script src="js/app.min.js"></script>

</body>
</html>