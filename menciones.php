<?php include("header.php");?>

<section class="menciones">
	<div class="container">
		
			<span class="title">Menciones legales</span>
			<span class="subtitle-simple">Declaraciones</span><p>ARTÍCULOS DEPORTIVOS DECATHLON S.A. de C.V. , es una persona moral, constituida de conformidad con las leyes de los Estados Unidos Mexicanos, según consta en la Escritura Pública número 74,415 de fecha 27 de julio de 2015, otorgada ante la fe del Lic. Marco Antonio Ruíz Aguirre, Notario Público número 229, del Distrito Federal, inscrita en el Registro Público de la Propiedad y del Comercio del Distrito Federal, bajo el Folio Mercantil electrónico número 541307&shy;1. Su Registro Federal de Contribuyentes es ADD150727S34. Su domicilio el ubicado en Francisco Fagoaga 80, Col. San Miguel Chapultepec, C.P. 11850, México, Distrito Federal.</p>
			<p>El sitio <a href="http://www.decathlon.com.mx/" target="_blank">www.decathlon.com.mx</a> tiene como finalidad proporcionar información orientativa acerca de los artículos, servicios, actividades, promociones, que ARTÍCULOS DEPORTIVOS DECATHLON S.A. de C.V.. desarrolla y vende a través de dicho site. La compañía se reserva el derecho de modificar y/o actualizar el contenido/configuración y/o presentación de la página web en cualquier momento, teniendo en cuenta los legítimos intereses de los usuarios.</p>
			<span class="subtitle-simple">Contacto</span>
			<p>Los internautas pueden utilizar cualquiera de los siguientes medios para dirigir cualquier comunicación a esta parte</p>
			<ul>
				<li>Vía e-mail: contacto.mexico@decathlon.com</li>
				<li>Vía telefónica: A venir</li>
			</ul>
			<span class="subtitle-simple">Propiedad industrial e intelectual</span>
			<p>ARTÍCULOS DEPORTIVOS DECATHLON S.A. de C.V. es titular o licenciatario de todos los derechos de Propiedad Industrial e Intelectual contenidos en la web, incluyendo, sin efecto limitativo, las marcas, nombres comerciales, imágenes, diseños gráficos o fotográficos, archivos, sonidos y, en general, todo el contenido que sea accesible desde el portal. Queda prohibida la cesión, distribución, transmisión, almacenamiento, o comunicación total o parcial del contenido de las paginas del portal, salvo autorización expresa y escrita de ARTÍCULOS DEPORTIVOS DECATHLON S.A. de C.V.</p>
        	<span class="subtitle-simple">Protección de datos de carácter personal</span>
        	<p>Para el procedimiento de alta en los servicios gratuitos ofrecidos por ARTÍCULOS DEPORTIVOS DECATHLON S.A. de C.V., que requieran de suscripción previa o registro previo, es necesario recopilar datos personales del usuario.</p>
        	<p> El Aviso de Privacidad de DECATHLON se encuentra en el apartado con el mismo nombre dentro del sitio web <a href="http://www.decathlon.com.mx/" target="_blank">www.decathlon.com.mx.</a>
         	A través de su sitio web
			<a href="http://www.decathlon.com.mx/" target="_blank">www.decathlon.com.mx,</a>
			DECATHLON puede obtener diversa información del usuario que puede compilarse y fijarse en una base de datos para lo cual DECATHLON publica su “Aviso de Privacidad” en el sitio de
			<a href="http://www.decathlon.com.mx/" target="_blank">www.decathlon.com.mx,</a>
			el cual se recomienda consultar para información referente al tratamiento de los datos personales del usuario.
			</p><p>
			Para resolver cualquier duda sobre el tratamiento de datos personales y cualquiera referente a este aviso de privacidad, nos podrá contactar al correo:
			<a href="mailto: privacidad.mexico@decathlon.com">privacidad.mexico@decathlon.com</a>
			Si el usuario dispone de la Tarjeta Decathlon puede gestionar su información proporcionada así como la suscripción a las listas de correo de la empresa.
			</p>

		</div>

</section>
<?php include("footer.php");?>
	
